<?php

use SONFin\Plugins\ViewPlugin;
use SONFin\Plugins\DbPlugin;
use Zend\Diactoros\Response;
use Psr\Http\Message\ServerRequestInterface;
use SONFin\Application;
use SONFin\Plugins\RoutePlugin;
use SONFin\ServiceContainer;
use SONFin\Plugins\AuthPlugin;

require_once __DIR__ .'/../vendor/autoload.php';
require_once __DIR__ . '/../src/helpers.php';

/* Foi inicializado o container de servico */
$serviceContainer = new ServiceContainer();

/* Passou o container de servico para dentro da pasta application */
$app = new Application($serviceContainer);

/* Nesse momento plugamos a rota */
$app->plugin(new RoutePlugin());
$app->plugin(new ViewPlugin());
$app->plugin(new DbPlugin());
$app->plugin(new AuthPlugin());

/**
 * @param ServerRequestInterface $request
 */
$app->get('/home/{name}/{id}', function (ServerRequestInterface $request){
    $response = new Response();
    $response->getBody()->write("Response com emmiter do diactoros");
    return $response;
});

require_once __DIR__ . '/../src/controllers/category-costs.php';
require_once __DIR__ . '/../src/controllers/bill-receives.php';
require_once __DIR__ . '/../src/controllers/bill-pays.php';
require_once __DIR__ . '/../src/controllers/users.php';
require_once __DIR__ . '/../src/controllers/auth.php';

$app->start();