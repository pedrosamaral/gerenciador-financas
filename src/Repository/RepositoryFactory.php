<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 06/07/2017
 * Time: 22:12
 */

namespace SONFin\Repository;


class RepositoryFactory
{
    public static function factory(string $modelClass)
    {
        return new  DefaultRepository($modelClass);
    }
}