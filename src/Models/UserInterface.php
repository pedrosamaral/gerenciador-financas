<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 22/07/2017
 * Time: 01:03
 */

namespace SONFin\Models;


interface UserInterface
{
    public function getId():int;

    public function getFullname():string ;

    public function getEmail():string ;

    public function getPassword():string ;


}