<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/06/2017
 * Time: 20:30
 */

namespace SONFin\Models;


use Illuminate\Database\Eloquent\Model;

class BillPay extends Model
{
    // Mass Assignment
    protected $fillable = [
        'date_lauch',
        'name',
        'value',
        'user_id',
        'category_cost_id',

    ];
}