<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 18/07/2017
 * Time: 21:45
 */

namespace SONFin\View\Twig;


use SONFin\Auth\AuthInterface;
use Twig_Extension;
use Twig_Extension_GlobalsInterface;

class TwigGlobals extends Twig_Extension implements Twig_Extension_GlobalsInterface
{

    private $auth;
    /**
     * TwigGlobals constructor.
     * @param AuthInterface $auth
     */
    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
    }


    public function getGlobals()
    {
        return [
            'Auth'  =>  $this->auth
        ];
    }
}