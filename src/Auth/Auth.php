<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/07/2017
 * Time: 14:27
 */

namespace SONFin\Auth;


use SONFin\Models\UserInterface;

class Auth implements AuthInterface
{
    public function __construct(JasnyAuth $jasnyAuth)
    {
        $this->jasnyAuth = $jasnyAuth;
        $this->sessionStart();

    }

    function login(array $credentials): bool
    {
        list('email'    =>  $email, 'password'  =>  $password) = $credentials;
        return $this->jasnyAuth->login($email, $password)!== null;
    }
    function check(): bool
    {
        return $this->user()!==null;
    }
    function logout(): void
    {
        $this->jasnyAuth->logout();
    }
    public function user(): ?UserInterface
    {
        return $this->jasnyAuth->user();
    }

    public function hashPassword(string $password): string
    {
        return $this->jasnyAuth->hashPassword($password);
    }

    protected function sessionStart()
    {
        if (session_status() == PHP_SESSION_NONE){
            session_start();
        }
    }


}