<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/07/2017
 * Time: 14:23
 */
declare(strict_types=1);
namespace SONFin\Auth;


use SONFin\Models\UserInterface;

interface AuthInterface
{
    public function login(array $credentials): bool;

    public function check(): bool;

    public function logout(): void;

    public function hashPassword(string  $password): string;

    public function user(): ?UserInterface;
}