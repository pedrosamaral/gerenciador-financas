<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/07/2017
 * Time: 14:43
 */

namespace SONFin\Auth;


use Jasny\Auth;
use Jasny\Auth\User;
use Jasny\Auth\Sessions;
use SONFin\Repository\RepositoryInterface;

class JasnyAuth extends Auth
{
    use Sessions;

    /**
     * JasnyAuth constructor
    **/
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Fetch a user by ID
     *
     * @param int|string $id
     * @return User|null
    **/
    public function fetchUserById($id)
    {
        return $this->repository->find($id, false);
    }
    /**
     * Fetch a user by username
     *
     * @param int|string $username
     * @return User|null
    **/
    public function fetchUserByUsername($username)
    {
        return $this->repository->findByField('email', $username)[0];
    }

}